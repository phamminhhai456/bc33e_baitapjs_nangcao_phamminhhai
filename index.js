function getEle(n) {
  return document.querySelector(n);
}

//Bài 1
function inBangSo() {
  var ketqua = "";
  for (var i = 1; i < 100; i += 10) {
    for (var j = i; j < i + 10; j++) {
      ketqua += j + "  ";
    }
    ketqua += "<br/>";
  }

  getEle("#ketQuaBai1").innerHTML = ketqua;
}

//Bài 2
function checkSoNguyenTo(n) {
  if (n < 2) {
    return false;
  }
  var canBacHai = Math.sqrt(n);
  for (var i = 2; i <= canBacHai; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

function timSoNguyenTo(arr) {
  var mangSoNguyenTo = [];
  for (var i = 0; i < arr.length; i++) {
    if (checkSoNguyenTo(arr[i])) {
      mangSoNguyenTo.push(arr[i]);
    }
  }
  return mangSoNguyenTo;
}

var arrNumber = [];
function inSoNguyenTo() {
  if (getEle("#txt-number").value.trim() == "") {
    return;
  }

  var soNhap = getEle("#txt-number").value * 1;
  arrNumber.push(soNhap);
  getEle("#txt-number").value = "";

  var mangSoNguyenTo = timSoNguyenTo(arrNumber);

  getEle("#ketQuaBai2").innerHTML = /*html*/ `
    <div>
        <p> Mảng : ${arrNumber}</p>
        <p> Mảng số nguyên tố: ${mangSoNguyenTo}</p>
    </div>
  `;
}

//Bài 3

function hienThiKetQuaTong() {
  if (getEle("#txt-sonhap").value.trim() == "") {
    return;
  }

  var soNhap = getEle("#txt-sonhap").value * 1;
  getEle("#txt-sonhap").value = "";

  var tong = tinhTong(soNhap);

  getEle("#ketQuaBai3").innerHTML = `Tổng: ${tong}`;
}

function tinhTong(n) {
  var sum = 0;

  for (var i = 2; i <= n; i++) {
    sum += i;
  }

  sum = sum + 2 * (i - 1);

  return sum;
}

//Bài 4
function timUoc(n) {
  var mangUoc = [];
  for (var i = n; i > 0; i--) {
    if (n % i == 0) {
      mangUoc.push(i);
    }
  }
  return mangUoc;
}

function hienThiUoc() {
  if (getEle("#soCanTimUoc").value.trim() == "") {
    return;
  }

  var soNhap = getEle("#soCanTimUoc").value * 1;
  getEle("#soCanTimUoc").value = "";

  var mangUoc = timUoc(soNhap);

  getEle("#ketQuaBai4").innerHTML = /*html*/ `
        <div>
            <p> Ước số của ${soNhap} là : ${mangUoc}</p>
        </div>
      `;
}

//Bài 5
function reverseString(n) {
  var chuoi = "";
  for (var i = n.length - 1; i >= 0; i--) {
    chuoi += n[i];
  }

  return chuoi;
}

function hienThiSo() {
  if (getEle("#soCanDaoNguoc").value.trim() == "") {
    return;
  }

  var soNhap = getEle("#soCanDaoNguoc").value;
  getEle("#soCanDaoNguoc").value = "";

  var soDaoNguoc = reverseString(soNhap);

  getEle("#ketQuaBai5").innerHTML = /*html*/ `
        <div>
            <p> ${soDaoNguoc}</p>
        </div>
      `;
}

//Bài 6

function hienThiSoLonNhat() {
  var sum = 0;
  for (var i = 1; i < 100; i++) {
    sum += i;
    if (sum >= 100) {
      break;
    }
  }
  getEle("#ketQuaBai6").innerHTML = i - 1;
}

//Bài 7
function hienThiBangCuuChuong() {
  if (getEle("#soCuuChuong").value.trim() == "") {
    return;
  }

  var soNhap = getEle("#soCuuChuong").value * 1;
  getEle("#soCuuChuong").value = "";

  var ketQua = "";
  for (var i = 1; i <= 10; i++) {
    ketQua += soNhap + "x" + i + "=" + soNhap * i + "<br/><br/>";
  }

  getEle("#ketQuaBai7").innerHTML = ketQua;
}

//Bài 8

function chiaBai() {
  var players = [[], [], [], []];

  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  for (var i = 0; i < players.length; i++) {
    for (var j = i; j < cards.length; j += 4) {
      players[i].push(cards[j]);
    }
  }
  var contentHtml = "";
  for (var i = 0; i < players.length; i++) {
    var content = `<p> Player ${i + 1} : ${players[i]}</p>`;
    contentHtml += content;
  }
  getEle("#ketQuaBai8").innerHTML = contentHtml;
}

//Bài 9
function hienThiSoGaVaCho() {
  if (
    getEle("#soGaVaCho").value.trim() == "" ||
    getEle("#soChan").value.trim() == ""
  ) {
    return;
  }
  var tongGaCho = getEle("#soGaVaCho").value * 1;
  var soChan = getEle("#soChan").value * 1;

  getEle("#soGaVaCho").value = "";
  getEle("#soChan").value = "";
  var ketQua = "";

  for (var ga = 1; ga < tongGaCho; ga++) {
    for (var cho = 1; cho < tongGaCho; cho++) {
      if (ga + cho == tongGaCho && ga * 2 + cho * 4 == soChan) {
        ketQua = `Gà : ${ga} con;  Chó : ${cho} con`;
      }
    }
  }
  getEle("#ketQuaBai9").innerHTML = ketQua;
}

//Bài 10
function tinhGoc(hour, minutes) {
  var gocKimPhut = minutes * 6;
  var gocKimGio = hour * 30 + minutes * 0.5;
  var goc = Math.abs(gocKimGio - gocKimPhut);

  return Math.min(goc, 360 - goc);
}

function hienThiGoc() {
  var hour = getEle("#kimGio").value * 1;
  var minute = getEle("#kimPhut").value * 1;

  if (
    hour < 0 ||
    minute < 0 ||
    hour > 12 ||
    minute > 60 ||
    getEle("#kimGio").value.trim() == "" ||
    getEle("#kimPhut").value.trim() == ""
  ) {
    return;
  }

  if (hour == 12) hour = 0;
  if (minute == 60) {
    minute = 0;
    hour += 1;
    if (hour > 12) hour = hour - 12;
  }

  getEle("#kimGio").value = "";
  getEle("#kimPhut").value = "";

  var goc = tinhGoc(hour, minute);

  getEle("#ketQuaBai10").innerHTML = `${goc} độ`;
}
